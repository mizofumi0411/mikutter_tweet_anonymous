# -*- coding: utf-8 -*-
Plugin.create(:mikutter_tweet_anonymous) do
	command(
		:tweet_anonymous,
		name: '匿名',
		condition: Plugin::Command[:Editable],
		visible: false,
		role: :postbox
		) do |op|
		buff = Plugin.create(:gtk).widgetof(op.widget).widget_post.buffer
		buff.text = "？？？「#{buff.text}」"
		op.widget.post_it!
	end
end